## Version 4

includes the following icons:<br>

bettercounter, exoairplayer, nextcloud, nextcloudcookbook, nextcloudcookbookflutter,<br> nextcloudyaga, nexttracks, nicefeed, nlweer, notepad, notificationcron, notificationlog, <br>
ocr, odeon, oeffi, opencontacts, opendnsupdater, openhab, openlauncher, openmultimaps, <br>
openmensa, openscale, opentodolist, opentracks, osmand, p2play, pcapdroid, piwigo, <br>
postwriter, potatoproject notes, pro expense, pslab, pulsemusic, railwaystationphotos,<br> raisetoanswer, redmoon, rxdroid, scrambled exif, screenshot tile, sdbviewer, <br>
shadelauncher, shadowsocks foss, shopwithmom, simpletask, skytube, snsmatrix, <br>
streetcomplete, suntimes, superfreeze, syncopoli, taupunkt, teddyforreddit, termbin, <br>
termux, texttorch, timelimit, timetable, tincapp, tinykeepass, torrentclient, <br>
trackercontrol, transportr, trezormanager, trigger, tschernobyl, tusky, <br>
tiny weather forecast germany, twitlatte, ulogger, unit converter ultimate, <br>
unstoppable, vanillamusic, vector pinball, vertretungsplan, vespucci, vocabletrainer,<br> volumecontrol, volumenotification, wakelock, wanidoku, wifianalyzer, wifi info,<br> 
x11basic, xmppadmin, zapp, zimlauncher<br>

## Version 3

includes the following icons:<br>

fairemail, fdroid, fedilab, feeder, fissh, fitotrack, florisboard, fly, forecasttie, <br>
forrunners, fullcolemak, fyreplace, gadgetbridge, geonotes, gitnex, greenbitcoinwallet, <br>
homeapp, homebot, hourlyreminder, huewidgets, hypezihg, joplin, kisslauncher, kodi, <br>
kontalk, kore, koyuspace, krautschlüssel, librehome, libreipsum, libretrivia,<br> lightandroidlauncher, log28, loophabbittracker, lrceditor, manyverse, markor,<br> 
masstransfer, materialfiles, metagersuche, mifareclassictool, minivector, mobilizon,<br> 
monerujo, moneytracker, monochromatic, morse, movim, musicalnotes, newpipe<br>

## Version 2

fixes display of incorrectly displayed icons:<br>

androidversion, anothernotes, appsmonitor, bitcoinpluslightningwallet, calculatorplusplus<br> 

## Version 1

the first version includes the following icons:<br>

1list, 9p, aegis, aicamera, androidversion, anonaddy, anothernotesv, appmonitor, archpakages<br> audioserve, babydots, balancetheball, baresip, binaryeye, bitcoinpluslightningwallet,<br> 
blockpuzzle, bookreader, briar, calculatorplusplus, callrecorder, campfire, catima<br> 
chubbyclick, clover, collaboraoffice, contactdiary, cryptolitycs, currencies, cyberwow<br> dailydozen, dandelior, dapnet, derdiedas, deufeitage, diab, diary, dmsexplorer, easynotesbr<br>
element, emeraldialer, enrecipes, eprssreader, escapepod, etar, etesync, subz<br>